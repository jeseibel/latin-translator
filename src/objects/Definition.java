package objects;

public class Definition
{
	public String latinWord;
	
	public PartOfSpeech partOfSpeech;
	
	public String synonyms[];
	
	public String parsings[]; // with 0 being the header	
	
	/**
	 * default constructor
	 */
	public Definition()
	{
		
	}
	
	/**
	 * Constructor
	 * @param newPartOfSpeech
	 * @param newDef
	 * @param newParsings
	 */
	public Definition(String newLatinWord, PartOfSpeech newPartOfSpeech,String newDef[], String newParsings[])
	{
		latinWord = newLatinWord;
		partOfSpeech = newPartOfSpeech;
		synonyms = newDef;
		parsings = newParsings;
	}

	@Override
	public String toString()
	{
		return latinWord + " - " + partOfSpeech + " - " + synonyms[0];
	}
}
