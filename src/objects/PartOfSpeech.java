package objects;

/**
 * used to store which part of speech this word is.
 * Unknown = -1
 * noun = 0
 * verb = 1
 * adjective = 2
 * adverb = 3
 * pronoun = 4
 * conjunction = 5
 * Preposition = 6
 */
public enum PartOfSpeech
{
	UNKOWN(-1),
	NOUN(0),
	VERB (1),
	ADJECTIVE(2),
	ADVERB(3),
	PRONOUN(4),
	CONJUNCTION(5),
	PREPOSITION(6),
	NUMERAL(7),
	QUESTION(8);
	// TODO add Interjection
	
	@SuppressWarnings("unused")
	private final int index;
	
	PartOfSpeech(int newIndex)
	{ index = newIndex; }
}
