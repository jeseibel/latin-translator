package objects;

import java.util.ArrayList;

public class Word
{
	public String latinWord;
	
	public ArrayList<Definition> definitions;
	
	public Word()
	{
		definitions = new ArrayList<>();
	}
	
	public Word(String NewLatinWord)
	{
		latinWord = NewLatinWord;
		definitions = new ArrayList<>();
	}
	
	@Override
	public String toString()
	{
		return latinWord;
	}
	
	/**
	 * TODO write description
	 * 
	 * @return
	 */
	public String getExportString()
	{
		String s = "";
		
		// start of the new word
		s += "NEW_WORD\n";
		s += latinWord + "\n";
		
		// save each definition
		for(Definition d : definitions)
		{
			s += "\n";
			
			// start of the new definition
			s += "START_DEF\n";
			
			// part of speech
			s += "PARTOFSPEECH\n";
			s += d.partOfSpeech + "\n";
			
			// start of each synonym
			s += "START_SYNONYMS\n";
			// each synonym will be on a new line
			for(String syn : d.synonyms)
				s += syn + "\n";
			// end of the synonyms
			s += "END_SYNONYMS\n";
			
			
			// start of each possible parsing
			s += "START_PARSINGS\n";
			// each parsing will be on a new line
			for(String par : d.parsings)
				s += par + "\n";
			// end of the possible parsings
			s += "END_PARSINGS\n";
			
			s += "END_DEF\n";
		}
		
		s += "END_WORD";
		
		
		return s;
	}
}
