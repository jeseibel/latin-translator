package interpreter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.TreeMap;

import javax.management.InstanceNotFoundException;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import objects.Definition;
import objects.PartOfSpeech;
import objects.Word;

/**
 * TODO add the ability to search Whitaker's words 
 * 		so we can find gerunds
 * TODO add gerund * 
 * 
 * @author James_Seibel
 * @version 2-19-2020
 */
public class DictionaryReader
{
	private WebDriver driver;
	
	private TreeMap<String,Word> wordCache;
	
	/**
	 * 
	 * @throws ConnectException if the user doesn't have Firefox or Chrome 79, 80, or 81 installed 
	 */
	public DictionaryReader() throws ConnectException
	{
		wordCache = new TreeMap<>();
		
		// TODO make this nicer
		determineBrowser();
		
		
		// if this file exists read in the words it contains
		readDictonaryFile("LatinWordList.txt");
	}
	
	/**
	 * TODO make nicer/refactor
	 * @throws ConnectException
	 */
	private void determineBrowser() throws ConnectException
	{
		// determine the OS
		String OS = null;
		if(OS == null) { OS = System.getProperty("os.name"); }
		
		
		try
		{
			System.out.println("Launching Firefox...");
			
			if(OS.startsWith("Windows"))
			{
				// this is required to open firefox
				System.setProperty("webdriver.gecko.driver","exefiles\\geckodriver.exe");
			}
			else
			{
				//TODO
				System.out.println("Firefox on Mac not currently supported...");
				driver = null;
				
				throw new Exception();
			}
			
			// print the log to a text file instead of to the console
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"log.txt");
			
			// launch browser
			driver = new FirefoxDriver();
			
			hideBrowser();
			
			// test to see if this driver works
			driver.get("https://www.google.com");
			
			return;
		}
		catch(Exception e1)
		{
			System.out.println("Firefox not installed... trying chrome version 81");
			
			try
			{
				if(OS.startsWith("Windows"))
				{
					// this is required to open the browser
					System.setProperty("webdriver.chrome.driver", "exefiles\\chromedriver81.exe");
				}
				else
				{
					// this is required to open the browser
					System.setProperty("webdriver.chrome.driver", "macOSfiles\\chromedriver81");
				}
				
				// launch browser
				driver = new ChromeDriver();
				
				hideBrowser();
				
				// test to see if this driver works
				driver.get("https://www.google.com/");
				
				//return;
			}
			catch(Exception e2)
			{
				System.out.println("chrome version 81 not installed... trying chrome version 80");
				
				try
				{
					if(OS.startsWith("Windows"))
					{
						// this is required to open the browser
						System.setProperty("webdriver.chrome.driver", "exefiles\\chromedriver80.exe");
					}
					else
					{
						// this is required to open the browser
						System.setProperty("webdriver.chrome.driver", "macOSfiles\\chromedriver80");
					}
					
					// launch browser
					driver = new ChromeDriver();
					
					hideBrowser();
					
					// test to see if this driver works
					driver.get("https://www.google.com/");
					
					return;
				}
				catch(Exception e3)
				{
					System.out.println("chrome version 80 not installed... trying chrome version 79");
					
					try
					{
						if(OS.startsWith("Windows"))
						{
							// this is required to open the browser
							System.setProperty("webdriver.chrome.driver", "exefiles\\chromedriver79.exe");
						}
						else
						{
							// this is required to open the browser
							System.setProperty("webdriver.chrome.driver", "macOSfiles\\chromedriver79");
						}
						
						// launch browser
						driver = new ChromeDriver();
						
						hideBrowser();
						
						// test to see if this driver works
						driver.get("https://www.google.com/");
						
						return;
					}
					catch(Exception e4)
					{
						System.out.println("No compatable browser detected... ");
						System.out.println("Please download either the latest version of Firefox or Chrome and run the program again\n");
						
						throw new ConnectException();
					}
				}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	/**
	 * connects to the dictionary page, and if the page is unavailable throws an exception
	 * @throws ConnectException the webpage is unavailable
	 */
	public void startProgram() throws ConnectException
	{
		try
		{
			// go to the dictionary page
			driver.get("https://www.latin-english.com/");
		}
		catch(Exception e)
		{
			// close and delete the driver
			// (since closing it doesn't set it to null)
			if(driver != null)
			{
				driver.close();
				driver = null;
			}
			
			throw new ConnectException();
		}
		
	}
	
	/**
	 * Return the word object for the given latin word.
	 * @param latinWord
	 * @return a word object that relates to the given latinWord
	 * @throws InstanceNotFoundException if the word isn't found in the online or local dictionary
	 */
	public Word findWord(String latinWord) throws InstanceNotFoundException
	{
		Word poss = null;
		
		// search for word in localDictionaray
		poss = wordCache.get(latinWord);
		
		if(poss != null)
			return poss;
		
		// we don't have the word locally, look it up online
		try
		{
			poss = searchOnlineForWord(latinWord);
		}
		catch(InstanceNotFoundException e)
		{
			throw new InstanceNotFoundException("\"" + latinWord + "\" wasn't found.");
		}
		
		// store the newly found word
		wordCache.put(latinWord, poss);
		
		return poss;
	}
	
	/**
	 * Search the online dictionary for the given latinWord.
	 * @param latinWord
	 * @exception InputMismatchException is thrown if the word isn't found on the website.
	 */
	private Word searchOnlineForWord(String latinWord) throws InstanceNotFoundException
	{
		// go to the dictionary page
		driver.get("https://www.latin-english.com/latin/" + latinWord);
		
		int numbOfResults = 0;
		Word newWord = new Word(latinWord);
		
		// make sure that the page loaded correctly
		try
		{
			// find how many possible words this could be
			numbOfResults = findNumberOfpossibleDefs();
		}
		catch(Exception e)
		{
			// if this fails that means this is one of the words that crashes the website
			return createDummyWord(latinWord);
		}
		
		if(numbOfResults == 0)
			return createDummyWord(latinWord);
		//throw new InstanceNotFoundException(latinWord + " doesn't exist in the online dictionary.");
		
		// for each possible result add a definition object
		for(int i = 1; i <= numbOfResults; i++)
		{
			try
			{
				//Noun III Declension Common (masculine and/or feminine)
				String titleString = getWordInfo(i);
				
				PartOfSpeech partOfSpeech = determinePartOfSpeech(titleString);
				
				String defList[] = getWordSynonyms(i);
				
				String parseList[] = getWordParsings(i);
				
				Definition def = new Definition(latinWord,partOfSpeech,defList,parseList);
				
				newWord.definitions.add(def);
			}
			catch(Exception e)
			{
				// in case the number of results was invalid
				// for example:
				// for some reason multa and multas start at 2...
			}
		}
		
		return newWord;
	}
	
	/**
	 * Create a blank word object. This is only used to 
	 * prevent a word that isn't available on the dictionary
	 * from being searched again. 
	 * @param latinWord
	 * @return a word object with only a latinWord filled in
	 */
	private Word createDummyWord(String latinWord)
	{
		Word newWord = new Word(latinWord);
		
		PartOfSpeech partOfSpeech = PartOfSpeech.UNKOWN;
		
		String defList[] = {""};
		
		String parseList[] = {""};
		
		Definition def = new Definition(latinWord,partOfSpeech,defList,parseList);
		
		newWord.definitions.add(def);
		
		return newWord;
	}
	
	/**
	 * Find which part of speech the titleString contains. 
	 * @param titleString
	 * @return the PartOfSpeech
	 */
	private PartOfSpeech determinePartOfSpeech(String titleString)
	{
		titleString = titleString.toLowerCase();
		
		if(titleString.contains("adjective"))
			return PartOfSpeech.ADJECTIVE;
		
		if(titleString.contains("adverb"))
			return PartOfSpeech.ADVERB;
		
		if(titleString.contains("conjunction"))
			return PartOfSpeech.CONJUNCTION;
		
		if(titleString.contains("pronoun"))
			return PartOfSpeech.PRONOUN;
		
		if(titleString.contains("noun"))
			return PartOfSpeech.NOUN;
		
		if(titleString.contains("verb"))
			return PartOfSpeech.VERB;
		
		if(titleString.contains("preposition"))
			return PartOfSpeech.PREPOSITION;
		
		if(titleString.contains("numeral"))
			return PartOfSpeech.NUMERAL;
		
		if(titleString.contains("question"))
			return PartOfSpeech.QUESTION;
		
		// we don't know what word this is
		return PartOfSpeech.UNKOWN;
	}
	
	/**
	 * Get the word definitions from the webpage.
	 * @param wordIndex the index of which definition should be used
	 * @return an array containing synonyms
	 */
	private String[] getWordSynonyms(int wordIndex) 
	{
		//TODO add duplication protection
		// example: furem
		
		WebElement elem = driver.findElement(By.xpath(
				"/html"		// look in the HTML code
				+ "/body"	// webpage
				+ "/div"	// everything but the header
				+ "/div"	// box everything fits in
				+ "/div"	// actual text
				+ "/div[3]"	// 1 = search bar, 2 = how many results and what was searched, 3 = results
				+ "/div["+ wordIndex +"]" // results
				+ "/div[2]"	// 1 = header, 2 = content
				+ "/div"	// actual text
				+ "/ul"		// definition table
				));
		
		String s = elem.getText();
		
		int numbOfLines = 1;
		for(int i = 0; i < s.length(); i++)
			if(s.charAt(i) == '\n')
				numbOfLines++;
		
		// create a normal array to return
		String returnArray[] = new String[numbOfLines];
		
		int indexes[] = findCharIndexes('\n',s);
		
		for(int i = 0; i < numbOfLines; i++)
			returnArray[i] = s.substring(indexes[i] + 1,indexes[i+1]);
		
		return returnArray;
	}
	
	/**
	 * returns an array of -1 and the location of every char
	 * in the given string.
	 * @param charToFind
	 * @param s
	 * @returns an array of int locations
	 */
	public int[] findCharIndexes(char charToFind,String s)
	{
		ArrayList<Integer> indexes = new ArrayList<>();
		indexes.add(-1);
		
		int nextLine = -1;
		// look through the string and find each new line character
		for(int i = 0; i < s.length(); i++)
		{
			// find the first tab character after i
			nextLine = s.indexOf(charToFind, i);
			
			if(nextLine != -1)
			{
				// we haven't reached the end yet, 
				// add this tab to the arrayList and keep going
				i = nextLine;
				indexes.add(nextLine);
			}
			else
			{
				// we have reached the end,
				// add the end of the line to the index list and stop the loop
				indexes.add(s.length());
				i = s.length();
			}
		}
		
		int returnArray[] = new int[indexes.size()];
		for(int i = 0; i < indexes.size(); i++)
			returnArray[i] = indexes.get(i);
		
		return returnArray;
	}
	
	/**
	 * Get the possible word parsings from the webpage.
	 * @param wordIndex
	 * @return
	 */
	private String[] getWordParsings(int wordIndex)
	{
		Exception exc = null;
		int parseIndex = 1;
		ArrayList<String> parseList = new ArrayList<String>();
		
		// get the header to the parsings		
		WebElement elem = driver.findElement(By.xpath(
				"/html"			// look in the HTML code
				+ "/body"		// webpage
				+ "/div"		// everything but the header
				+ "/div"		// box everything fits in
				+ "/div"		// actual text
				+ "/div[3]"		// 1 = search bar, 2 = how many results and what was searched, 3 = results
				+ "/div[" + wordIndex + "]"	// results
				+ "/div[2]"		// 1 = header, 2 = content
				+ "/div"		// actual text
				+ "/table[1]"	// parsing table
				+ "/thead"		// table header
				+ "/tr"			// header text
				));
		
		parseList.add(elem.getText());
		
		// get the possible parsings		
		while(exc == null) // keep going down the table until we go off
		{
			try
			{			
				elem = driver.findElement(By.xpath(
						"/html"		// look in the HTML code
						+ "/body"		// webpage
						+ "/div"			// everything but the header
						+ "/div"			// box everything fits in
						+ "/div"			// actual text
						+ "/div[3]"		// 1 = search bar, 2 = how many results and what was searched, 3 = results
						+ "/div[" + wordIndex + "]"	// results
						+ "/div[2]"		// 1 = header, 2 = content
						+ "/div"			// actual text
						+ "/table[1]"		// parsing table
						+ "/tbody"			// table body
						+ "/tr[" + parseIndex + "]"
						));
				
				parseList.add(elem.getText());
			}
			catch(Exception e)
			{
				// save the exception
				exc = e;
			}
			
			parseIndex++;
		}
		
		// create a normal array to return
		String returnArray[] = new String[parseList.size()];
		for(int i = 0; i < parseList.size(); i++)
			returnArray[i] = parseList.get(i);
		
		
		return returnArray;
	}
	
	
	/**
	 * Returns a string like this:
	 * "Noun III Declension Common (masculine and/or feminine)" 
	 * @param wordIndex which definition to use
	 */
	private String getWordInfo(int wordIndex)
	{
		try
		{
			WebElement elem = driver.findElement(By.xpath(
					"/html"		// look in the HTML code
					+ "/body"	// webpage
					+ "/div"	// everything but the header
					+ "/div"	// box everything fits in
					+ "/div"	// actual text
					+ "/div[3]"	// 1 = search bar, 2 = how many results and what was searched, 3 = results
					+ "/div["+ wordIndex +"]" // results
					+ "/div[2]"	// 1 = header, 2 = content
					+ "/div"	// actual text
					+ "/h3"		// type of word (noun, adj, etc.), declension, gender
					));
			
			return elem.getText();
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		
		return "";
	}
	
	/**
	 * Returns an int for how many different definitions the online dictionary found.
	 * NOTE: this isn't always 100% accurate; sometimes the website doesn't have
	 * the correct number listed, for example multas says it has 3 definitions but only has 2. 
	 * @return the number online definitions
	 */
	private int findNumberOfpossibleDefs()
	{
		
		WebElement elem = driver.findElement(By.xpath(
				"/html"	// look in the HTML code
				+ "/body"	// webpage
				+ "/div"	// everything but the header
				+ "/div"	// box everything fits in
				+ "/div"	// actual text
				+ "/div[2]"	// 1 = search bar, 2 = how many results and what was searched, 3 = results
				+ "/div" 	// box everything fits in
				+ "/div"	// alert box
				+ "/div"	// alert text
				+ "/b"		// number of results found
				));
		
		return Integer.parseInt(elem.getText());
	}
	
	/**
	 * Close the web browser
	 */
	public void closeBrowser()
	{
		if(driver != null)
			driver.quit();
	}
	
	/**
	 * Hide the browser off screen.
	 */
	public void hideBrowser()
	{
		driver.manage().window().setPosition(new Point(-2000, 0)); // move the window off screen
	}
	
	/**
	 * Bring the browser on screen.
	 */
	public void showBrowser()
	{
		driver.manage().window().setPosition(new Point(0, 0)); // move the window off screen
	}
	
	/**
	 * returns if the webDriver exists or not,
	 * if it doesn't exist then the browser isn't open.
	 * @return true or false
	 */
	public boolean doesDriverExist()
	{
		return driver != null;
	}
	
	
	/**
	 * Read in the dictionary file and add each word to the
	 * local storage.
	 * @param fileName the name of the file to write to (including extension)
	 */
	public void readDictonaryFile(String fileName)
	{
		System.out.println("file reading starting... ");
		
		int numbOfWords = 0;
		
		// look for the file
		File file = new File(fileName);
		
		if(file.exists())
		{
			// read
			try {
				
				// create the reader
				BufferedReader in = new BufferedReader(new FileReader(file));
				
				
				String s;
				Word newWord = null;
				Definition def = null;
				ArrayList<String> synList = null; 
				ArrayList<String> parseList = null;
				
				// debugging variables
				int line = 0;
				// this is used to make sure that every word
				// has a NEW_WORD and END_WORD token
				boolean readingWord = false;
				
				while((s = in.readLine()) != null)
				{
					switch(s)
					{
					case"NEW_WORD":
						s = in.readLine(); // get the latin word
						line++;
						newWord = new Word(s);
						
						// the last word didn't have a closing END_WORD
						// throw an error
						if(readingWord)
							throw new IOException("ERROR on line: " + line + "\n"
									+ "Expected: END_WORD token!");
							
						readingWord = true;
						break;
					case"END_WORD":
						// store the newly read word
						wordCache.put(newWord.latinWord, newWord);
						numbOfWords++;
						System.out.println(numbOfWords + ". \"" + newWord.latinWord + "\" added.");
						readingWord = false;
						break;
						
					case"START_DEF":
						if(!readingWord)
							throw new IOException("ERROR on line: " + line +"\n"
									+ "Expected: NEW_WORD, but got START_DEF!");
						
						def = new Definition();
						def.latinWord = newWord.latinWord;
						break;
					case"END_DEF":
						if(!readingWord)
							throw new IOException("ERROR on line: " + line +"\n"
									+ "Expected: NEW_WORD, but got END_DEF!");
						
						newWord.definitions.add(def);
						break;
						
					case"PARTOFSPEECH":
						if(!readingWord)
							throw new IOException("ERROR on line: " + line +"\n"
									+ "Expected: NEW_WORD, but got PARTOFSPEECH!");
						
						s = in.readLine();
						line++;
						def.partOfSpeech = determinePartOfSpeech(s);
						break;
						
					case"START_SYNONYMS":
						if(!readingWord)
							throw new IOException("ERROR on line: " + line +"\n"
									+ "Expected: NEW_WORD, but got START_SYNONYMS!");
						
						synList = new ArrayList<>();
						s = in.readLine(); // go to the next line after "START_SYNONYMS"
						line++;
						while(!s.equals("END_SYNONYMS"))
						{
							// read in each synonym
							synList.add(s);
							s = in.readLine();
							line++;
						}
						def.synonyms = synList.toArray(new String[0]);
						break;
						
					case"START_PARSINGS":
						if(!readingWord)
							throw new IOException("ERROR on line: " + line +"\n"
									+ "Expected: NEW_WORD, but got START_PARSINGS!");
						
						parseList = new ArrayList<>();
						s = in.readLine(); // go to the next line after "START_PARSINGS"
						line++;
						while(!s.equals("END_PARSINGS"))
						{
							parseList.add(s);
							s = in.readLine();
							line++;
						}
						def.parsings = parseList.toArray(new String[0]);
						break;
					}
					
					line++;
				}
				
				in.close();
			}
			catch(FileNotFoundException e)
			{
				System.out.println("ERROR: file not found or inaccesable\n");
				e.printStackTrace();
			}
			catch(IOException e)
			{
				System.out.println("ERROR: file reading failed\n");
				e.printStackTrace();
			}
			
		}
		else
		{
			System.out.println("No dictionary file found.\n"
					+ "One will be created on exit.");
		}
		
		System.out.println("File reading completed.");
		System.out.println(numbOfWords + " word(s) read");
	}
	
	/**
	 * Write each word in local storage to a file
	 * @param fileName the name of the file to write to (including extension)
	 * @throws IOException if the file can't be created
	 */
	public void writeDictionaryToFile(String fileName) throws IOException
	{
		System.out.println("Beginning file writting...");
		
		File file = new File("./" + fileName);
		
		if(!file.exists())
			file.createNewFile();
		
		PrintWriter outFile = new PrintWriter (file);
		
		int numbWords = 0;
		
		// go through each word in the tree 
		for(Map.Entry<String,Word> entry : wordCache.entrySet())
		{
			Word w = entry.getValue();
			
			numbWords++;
			System.out.println(numbWords + ". \"" + w.toString() + "\" written.");
			outFile.println(w.getExportString() + "\n\n\n");
		}
		
		// close and write the file
		outFile.close();
		
		System.out.println("File writting complete");
		System.out.println(numbWords + " words written.");
	}
}
