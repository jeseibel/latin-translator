package driver;
/**
 * https://stackoverflow.com/questions/48682546/how-to-kill-drivers-after-running-the-automation-scripts
 * 
 * 
 * TODO if the file reading fails because of a bad file, crash the program, and don't overwrite the file.
 * @author James_Seibel
 * @version 2-19-2020
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.util.function.UnaryOperator;

import javax.management.InstanceNotFoundException;

import gui.WordPane;
import interpreter.DictionaryReader;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Latin_Driver extends Application implements EventHandler<ActionEvent>
{
	public static Stage gui;

	public static BorderPane root;


	// GUI elements
	public static TextArea searchBar;

	public static Button submitButton;
	public static Button clearButton;
	public static Button browserButton;

	public static boolean browserVisible = false;

	public static ScrollPane resultScroll;
	public static GridPane resultPane; 

	public static Text errorText;

	public static Alert loadingMessageBox;

	// other variables
	public static DictionaryReader reader;

	public static int numbOfWords = 0;
	public static String errorString = "";

	public static final int BUTTONHEIGHT = 40;
	public static final int MAXNUMBWIDE = 4;


	@Override
	public void start(Stage primaryStage) 
	{

		root = new BorderPane();

		// set what happens when the program is closed
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() 
		{
			@Override
			public void handle(WindowEvent we) 
			{
				shutdown();
			}
		}); 

		// create the scene
		Scene scene = new Scene(root,(WordPane.WIDTH * MAXNUMBWIDE) + 4,320);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		// set what is to be displayed
		primaryStage.setScene(scene);
		primaryStage.show();

		// title the window
		primaryStage.setTitle("Latin translator 1.4");
		
		try {
			primaryStage.getIcons().add(new Image(new FileInputStream("./program_icon.png")));
		} catch (FileNotFoundException e1) {
			// if not found ignore
		}
		
		// set the variable so we can edit this outside the start method
		gui = primaryStage;

		// create the rest of the GUI
		createGUI(gui);

		// show a loading screen to let the user know what is happening
		createLoadingAlert();
		loadingMessageBox.show();

		try
		{
			// create the reader object
			reader = new DictionaryReader();
		}
		catch(ConnectException e)
		{
			//TODO
		}

		try
		{
			runProgram();
		}
		catch(ConnectException e)
		{
			Text t = new Text();
			t.setText("ERROR: could not connect to: www.latin-english.com\n\n"
					+ "This either means that firefox is updating,\n"
					+ "\tWhich if that is the case, please wait for it to finish,\n"
					+ "\tclose the browser and reboot this program.\n\n"
					+ "Or it means this computer is not connected to the internet,\n"
					+ "please connect to the internet, and reboot this program.\n");
			resultPane.add(t, 0, 0);

			searchBar.setDisable(true);
			submitButton.setDisable(true);
			clearButton.setDisable(true);
			browserButton.setDisable(true);

			System.out.println("ERROR: could not connect to: www.latin-english.com\n");
		}

		loadingMessageBox.close();
	}

	public static void main(String[] args)
	{
		launch(args);
	}

	/**
	 * run the reader's start program method.
	 * @throws ConnectException 
	 */
	public void runProgram() throws ConnectException  
	{
		reader.startProgram();
	}

	/**
	 * Set up the initial GUI.
	 * @param primaryStage
	 */
	private void createGUI(Stage primaryStage)
	{
		GridPane menuPane = new GridPane();
		root.setTop(menuPane);

		// add the clear button
		addClearButton(menuPane, 1, 0);

		// add a magnifying glass icon
		addSearchIcon(menuPane, 1, 0);

		// add a text area for typing in words
		addSearchBar(menuPane, 2, 0);

		// add a submit button
		addSubmitButton(menuPane, 3, 0);

		// add a button to hide and show the browser
		//addBrowserButton(menuPane, 4, 0);


		// add a grid pane within a scroll pane for the wordPanes
		resultScroll = new ScrollPane();
		resultPane = new GridPane();
		resultScroll.setContent(resultPane);
		root.setCenter(resultScroll);

		// add the error text to the bottom
		errorText = new Text();
		root.setBottom(errorText);
	}

	/**
	 * Add a magnifying glass icon at the designated location to the designated gridPane.
	 * @param menuPane the gridPane to add the icon too
	 * @param xLocation the x location in the gridPane
	 * @param yLocation the y location in the gridPane
	 */
	private void addSearchIcon(GridPane menuPane, int xLocation, int yLocation)
	{
		try
		{
			Image searchIcon = new Image(new FileInputStream("./search_icon.png"));
			ImageView icon = new ImageView(searchIcon);
			icon.setFitHeight(BUTTONHEIGHT);
			icon.setFitWidth(BUTTONHEIGHT);

			menuPane.add(icon, xLocation, yLocation);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Add the clear button to the menuPane at the designated location.
	 * @param menuPane the gridPane to add the icon too
	 * @param xLocation the x location in the gridPane
	 * @param yLocation the y location in the gridPane
	 */
	private void addSubmitButton(GridPane menuPane, int xLocation, int yLocation)
	{
		submitButton = new Button();
		submitButton.setText("submit");
		submitButton.setOnAction(this);
		submitButton.setMinWidth(70);
		submitButton.setMaxWidth(70);

		menuPane.add(submitButton, xLocation, yLocation);
	}

	/**
	 * Add the clear button to the menuPane at the designated location.
	 * @param menuPane the gridPane to add the icon too
	 * @param xLocation the x location in the gridPane
	 * @param yLocation the y location in the gridPane
	 */
	private void addClearButton(GridPane menuPane, int xLocation, int yLocation)
	{
		clearButton = new Button();
		clearButton.setText("clear");
		clearButton.setOnAction(this);
		clearButton.setMinWidth(70);
		clearButton.setMaxWidth(70);

		menuPane.add(clearButton, 0, 0);
	}

	/**
	 * Add the clear button to the menuPane at the designated location.
	 * @param menuPane the gridPane to add the icon too
	 * @param xLocation the x location in the gridPane
	 * @param yLocation the y location in the gridPane
	 */
	//	private void addBrowserButton(GridPane menuPane, int xLocation, int yLocation)
	//	{
	//		browserButton = new Button();
	//		browserButton.setText("Show Browser");
	//		browserButton.setOnAction(this);
	//		browserButton.setMinWidth(100);
	//		browserButton.setMaxWidth(100);
	//		
	//		menuPane.add(browserButton, xLocation, yLocation);
	//	}

	/**
	 * Add the clear button to the menuPane at the designated location.
	 * @param menuPane the gridPane to add the icon too
	 * @param xLocation the x location in the gridPane
	 * @param yLocation the y location in the gridPane
	 */
	private void addSearchBar(GridPane menuPane, int xLocation, int yLocation)
	{
		searchBar = new TextArea();

		searchBar.setMinHeight(BUTTONHEIGHT);
		searchBar.setMaxHeight(BUTTONHEIGHT);
		searchBar.setMinWidth(64*4);
		searchBar.setMaxWidth(3600);

		// only allow letters
		UnaryOperator<Change> filter = change -> {
			String text = change.getText();

			if(text.matches("[a-zA-Z ]*"))
				return change;
			return null;
		};

		TextFormatter<String> textFormatter = new TextFormatter<>(filter);
		searchBar.setTextFormatter(textFormatter);

		// when enter is pressed submit the search
		searchBar.setOnKeyPressed(new EventHandler<KeyEvent>()
		{
			@Override
			public void handle(KeyEvent e)
			{
				if(e.getCode() == KeyCode.ENTER)
					submitSearch();
			}
		});

		menuPane.add(searchBar, xLocation, yLocation);
	}

	/**
	 * Create the loading screen alert for later use
	 */
	private void createLoadingAlert()
	{
		loadingMessageBox = new Alert(AlertType.INFORMATION);

		loadingMessageBox.initOwner(gui);
		loadingMessageBox.setTitle("loading...");
		loadingMessageBox.setContentText("Program loading...\n"
				+ "The program will not respond untill it finishes.\n"
				+ "please wait...\n");
		loadingMessageBox.setHeaderText("");
	}

	/**
	 * Attempts to add the latinWord to the GUI
	 * @param latinWord
	 * @return a string with any error messages
	 */
	private String addWordToGUI(String latinWord)
	{
		try
		{
			WordPane wp = new WordPane(reader.findWord(latinWord));

			// using integer math determine where the new wordPane should go
			int x = numbOfWords % MAXNUMBWIDE;
			int y = numbOfWords / MAXNUMBWIDE;

			resultPane.add(wp, x, y);

			numbOfWords++;
		}
		catch(InstanceNotFoundException e)
		{
			return e.getMessage();
		}

		return "";
	}

	/**
	 * This takes the text from the searchBar and adds those words to the GUI.
	 */
	private void submitSearch()
	{
		//TODO allow for splitting words like 
		// vallemque into vallem and que
		// or taurosque into tauros and que

		String words[];
		String s = searchBar.getText();

		// only continue if the user has something in the search bar
		if(s.isEmpty())
			return;

		loadingMessageBox.show();

		// overwrite what was there before
		resultPane.getChildren().clear();
		errorText.setText("");
		errorString = "";
		numbOfWords = 0;

		String oldS = s;
		// replace duplicate spaces
		s = s.replaceAll("\\s+", " ");

		// only update the search bar if something changed
		// (this way the user's cursor shouldn't be reset every time they submit a search)
		if(!oldS.equals(s))
			searchBar.setText(s);

		// make sure that all the words we look for are the same
		// we don't need to worry about weather a word is capital or not
		s = s.toLowerCase();

		// find where the start and end of each word is
		int indexes[] = reader.findCharIndexes(' ',s);
		words = new String[indexes.length - 1];

		// separate each word
		for(int i = 0; i < indexes.length - 1; i++)
			words[i] = s.substring(indexes[i] + 1,indexes[i+1]);

		// add each word
		for(int i = 0; i < indexes.length - 1; i++)
		{
			String temp = addWordToGUI(words[i]);

			if(!temp.isEmpty())
				errorString += temp + "\n";
		}

		loadingMessageBox.hide();

		errorText.setText(errorString);
	}

	/**
	 * This method is called when the window is closed. 
	 */
	private void shutdown() 
	{
		System.out.println("Shutting down program...");

		if(reader != null)
		{
			// only close the browser if the browser exists
			if(reader.doesDriverExist())
				reader.closeBrowser();

			try
			{
				reader.writeDictionaryToFile("LatinWordList.txt");
			}
			catch (IOException e)
			{
				System.out.println("ERROR:");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void handle(ActionEvent e)
	{
		if(e.getSource().equals(submitButton))
		{
			submitSearch();
		}

		if(e.getSource().equals(clearButton))
		{
			searchBar.clear();
			resultPane.getChildren().clear();
			errorText.setText("");
			errorString = "";
			numbOfWords = 0;
		}

		if(e.getSource().equals(browserButton))
		{
			// if the browser exists, toggle weather it is visible
			// or not
			if(reader != null && reader.doesDriverExist())
			{
				browserVisible = !browserVisible;

				if(browserVisible)
				{
					reader.showBrowser();
					browserButton.setText("Hide Browser");
				}
				else
				{
					reader.hideBrowser();
					browserButton.setText("Show Browser");
				}
			}
		}
	}
}
