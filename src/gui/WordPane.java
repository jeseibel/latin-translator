package gui;

import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import objects.Word;

/**
 * This Pane is designed to display: a given latin word, it's possible synonyms, and possible parsings.
 * @author James_Seibel
 * @version 2-18-2020
 */
public class WordPane extends GridPane
{
	private Word word;
	
	private int selectedWordIndex = 0;
	
	public static final int WIDTH = 256;
	public static final int HEIGHT = 256;
	public static final int COMBOBOXHEIGHT = 64;
	
	public static final int TEXTHEIGHT = (HEIGHT - COMBOBOXHEIGHT) / 2;
	
	private ComboBox<String> wordCombo;
	private ScrollPane defPane;
	private ScrollPane parsePane;
	
	/**
	 * Constructor
	 * @param newWord
	 */
	public WordPane(Word newWord)
	{
		this.setPadding(new Insets(10, 10, 10, 10));
		
		this.setMinHeight(HEIGHT);
		this.setMaxHeight(HEIGHT);
		this.setMinWidth(WIDTH);
		this.setMaxWidth(WIDTH);
		
		word = newWord;
		
		createGUI();
	}
	
	/**
	 * create the initial GUI layout
	 */
	private void createGUI()
	{
		// dropdown list for the definitions with a different partOfSpeech
		addComboBox();
		
		// scroll box with definitions
		addDefenitionList();
		
		// parsing textbox
		addParseList();
	}
	
	/**
	 * this is called whenever we need to update this pane
	 */
	private void update()
	{
		updateDefinitionList();
		
		updateParseList();
	}
	
	/**
	 * Add the text which holds the possible parsings.
	 */
	private void addParseList()
	{
		parsePane = new ScrollPane();
		parsePane.setMaxHeight(TEXTHEIGHT);
		parsePane.setMinHeight(TEXTHEIGHT);
		
		Text parseText = new Text();
		parseText.setWrappingWidth(WIDTH);
		
		String s = "";
		for(int i = 0; i < word.definitions.get(selectedWordIndex).parsings.length; i++)
			s += word.definitions.get(selectedWordIndex).parsings[i] + "\n";
		parseText.setText(s);
		parsePane.setContent(parseText);
		this.add(parsePane, 0, 3);
	}
	
	/**
	 * update the possible parsing text to the new selected word.
	 */
	private void updateParseList()
	{
		Text parseText = new Text();
		String s = "";
		for(int i = 0; i < word.definitions.get(selectedWordIndex).parsings.length; i++)
			s += word.definitions.get(selectedWordIndex).parsings[i] + "\n";
		parseText.setText(s);
		
		parsePane.setContent(parseText);
	}
	
	/**
	 * Add a scrollPane with the possible definitions
	 */
	private void addDefenitionList()
	{
		// TODO add the ability to change things and then update the actual word
		
		defPane = new ScrollPane();
		defPane.setMaxWidth(WIDTH);
		defPane.setMinWidth(WIDTH);
		defPane.setMaxHeight(TEXTHEIGHT);
		defPane.setMinHeight(TEXTHEIGHT);
		
		Text defs = new Text();
		String definitions = "";
		for(int i = 0; i < word.definitions.get(selectedWordIndex).synonyms.length; i++)
			definitions += "* " + word.definitions.get(selectedWordIndex).synonyms[i] + "\n";
		defs.setText(definitions);
		defPane.setContent(defs);
		
		this.add(defPane, 0, 2);
	}
	
	/**
	 * update the possible definition list to the new selected word.
	 */
	private void updateDefinitionList()
	{
		Text defs = new Text();
		String definitions = "";
		for(int i = 0; i < word.definitions.get(selectedWordIndex).synonyms.length; i++)
			definitions += "* " + word.definitions.get(selectedWordIndex).synonyms[i] + "\n";
		defs.setText(definitions);
		defPane.setContent(defs);
	}
	
	/**
	 * Add the comboBox that holds the possible word definitions
	 */
	private void addComboBox()
	{
		wordCombo = new ComboBox<String>();
		for(int i = 0; i < word.definitions.size(); i++)
			wordCombo.getItems().add(word.definitions.get(i).toString());
		
		// credit to: Sai Dandem, at
		// https://stackoverflow.com/questions/55471652/javafx-combobox-textwrap
		// for the ability to text wrap
		wordCombo.setButtonCell(new ListCell<String>()
		{
            private Text textLbl;
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(null);
                if (!empty) {
                    getTextLbl().setText(item);
                    setGraphic(getTextLbl());
                }
            }

            private Text getTextLbl(){
                if(textLbl ==null){
                    textLbl = new Text();
                    textLbl.wrappingWidthProperty().bind(this.widthProperty().subtract(10));
                }
                return textLbl;
            }
        });
		
		wordCombo.setMaxWidth(WIDTH);
		wordCombo.setMinWidth(WIDTH);
		wordCombo.setMaxHeight(COMBOBOXHEIGHT);
		wordCombo.setMinHeight(COMBOBOXHEIGHT);
		
		wordCombo.setOnAction(Event -> {
			selectedWordIndex = wordCombo.getSelectionModel().getSelectedIndex();
			update();
		});
		
		if(word.definitions.size() == 1)
			// have the combo box slightly darker
			wordCombo.setStyle("-fx-background-color: linear-gradient(to bottom, #cccccc, #999999)");
		
		wordCombo.getSelectionModel().select(selectedWordIndex);
		this.add(wordCombo, 0, 0);
	}
		
	
}
